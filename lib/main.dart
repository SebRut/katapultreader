// Dart imports:
import 'dart:io';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:workmanager/workmanager.dart';

// Project imports:
import 'package:katapultreader/domain/repository/article_repository.dart';
import 'package:katapultreader/domain/service/background_notification_service.dart';
import 'package:katapultreader/route/pages.dart';

void callbackDispatcher() {
  Workmanager().executeTask((taskName, inputData) async {
    switch (taskName) {
      case BackgroundNotificationService.TASK_NAME:
        await Get.find<BackgroundNotificationService>().checkForArticles();
        break;
    }

    return Future.value(true);
  });
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initServices();

  runApp(MyApp());
}

Future<FlutterLocalNotificationsPlugin> initNotifications() async {
  final notificationsPlugin = FlutterLocalNotificationsPlugin();

  final androidSettings = AndroidInitializationSettings('launch_background');

  final initializationSettings =
      InitializationSettings(android: androidSettings);
  await notificationsPlugin.initialize(initializationSettings);

  return notificationsPlugin;
}

initServices() async {
  final notificationsEnabled = false;
  if ((Platform.isAndroid || Platform.isIOS) && notificationsEnabled) {
    await Get.putAsync(initNotifications, permanent: true);
    Get.put(
        BackgroundNotificationService(ArticleRepository(), Get.find()).init());
    await Workmanager().initialize(callbackDispatcher, isInDebugMode: false);
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      light: ThemeData(),
      dark: ThemeData.dark(),
      initial: AdaptiveThemeMode.system,
      builder: (darkTheme, theme) => GetMaterialApp(
        title: 'Katapult MV Reader',
        getPages: AppPages.routes,
        theme: theme,
        darkTheme: darkTheme,
        initialRoute: AppPages.INITIAL,
      ),
    );
  }
}
