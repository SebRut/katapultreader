// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:katapultreader/view/page/articles/articles_binding.dart';
import 'package:katapultreader/view/page/articles/articles_page.dart';
import 'package:katapultreader/view/page/dashbook/dashbook_page.dart';

part 'routes.dart';

class AppPages {
  static const INITIAL = Routes.ARTICLES;

  static final routes = [
    GetPage(
        name: Routes.ARTICLES,
        page: () => ArticlesPage(),
        binding: ArticlesBinding()),
    GetPage(name: Routes.DASHBOOK, page: () => DashbookPage())
  ];
}
