// Dart imports:
import 'dart:convert';

// Package imports:
import 'package:crypto/crypto.dart';
import 'package:html/dom.dart';

class ArticleModel {
  final String title;
  final Uri uri;

  final Uri? teaserImageUri;
  final String? dateline; // TODO meaning
  final String? teaser;
  final Uri? highResImageUri;

  String get id => sha256.convert(utf8.encode(uri.toString())).toString();

  ArticleModel(
      {required this.title,
      required this.uri,
      this.teaserImageUri,
      this.dateline,
      this.teaser,
      this.highResImageUri});

  factory ArticleModel.fromElement(Element element) {
    var titleElement = element.getElementsByClassName('grid-item-title').first;
    var title = titleElement.text.trim();
    var hrefText =
        titleElement.getElementsByTagName('a').first.attributes['href'];
    var uri = Uri.parse(hrefText!);

    var imgSplit = element
        .getElementsByTagName('img')
        .first
        .attributes['data-srcset']
        ?.split(' ');
    var teaserImageLinkText = imgSplit?.first;

    var teaserImageUri =
        teaserImageLinkText != null ? Uri.tryParse(teaserImageLinkText) : null;
    var highResImageLinkText = imgSplit?[imgSplit.length - 2];
    var highResImageUri = highResImageLinkText != null
        ? Uri.tryParse(highResImageLinkText)
        : null;

    var dateline =
        element.getElementsByClassName('grid-item-dateline').first.text.trim();
    var teaser =
        element.getElementsByClassName('grid-item-teaser').first.text.trim();

    return ArticleModel(
        title: title,
        uri: uri,
        teaserImageUri: teaserImageUri,
        dateline: dateline,
        teaser: teaser,
        highResImageUri: highResImageUri);
  }
}
