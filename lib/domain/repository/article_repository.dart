// Package imports:
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';

// Project imports:
import 'package:katapultreader/domain/model/article_model.dart';

class ArticleRepository {
  Future<List<ArticleModel>> fetchArticles() {
    return _getHtml().then((html) => _parseArticles(html));
  }

  Future<List<ArticleModel>> _parseArticles(Document html) {
    var articleElements = html.getElementsByClassName('grid-item');
    return Future.wait(articleElements.map((e) => _parseArticleElement(e)));
  }

  Future<Document> _getHtml() async {
    return await Client()
        .get(Uri.parse('https://katapult-mv.de/artikel'))
        .then((response) => response.body)
        .then((body) => parse(body));
  }

  Future<ArticleModel> _parseArticleElement(Element element) {
    return Future.microtask(() => ArticleModel.fromElement(element));
  }
}
