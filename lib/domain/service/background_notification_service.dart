// Package imports:
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:localstorage/localstorage.dart';
import 'package:workmanager/workmanager.dart';

// Project imports:
import 'package:katapultreader/domain/repository/article_repository.dart';

class BackgroundNotificationService extends GetxService {
  static const _LAST_ARTICLE_KEY = 'last_article';
  static const _STORAGE_NAME = 'background_service';
  static const TASK_NAME = 'fetch_new_articles';

  final ArticleRepository _articleRepository;
  final FlutterLocalNotificationsPlugin _notificationsPlugin;

  BackgroundNotificationService(
      this._articleRepository, this._notificationsPlugin);

  final _storage = LocalStorage(_STORAGE_NAME);

  init() {
    Workmanager().registerPeriodicTask('katapult_reader', TASK_NAME,
        frequency: Duration(hours: 1),
        existingWorkPolicy: ExistingWorkPolicy.replace,
        constraints: Constraints(networkType: NetworkType.connected));
  }

  Future<void> checkForArticles() async {
    final articles = await _articleRepository.fetchArticles();
    final newestArticleId = articles.first.id;

    await _storage.ready;
    final lastArticleId = _storage.getItem(_LAST_ARTICLE_KEY) ?? "";

    if (newestArticleId != lastArticleId) {
      await _onNewArticle(newestArticleId);
    }
  }

  Future<void> _onNewArticle(String newestArticleId) async {
    print("new article!");
    await _storage.setItem(_LAST_ARTICLE_KEY, newestArticleId);

    await _showNewArticlesNotification();
  }

  Future<void> _showNewArticlesNotification() async {
    const notificationDetails = NotificationDetails();
    await _notificationsPlugin.show(0, 'Test', 'new', notificationDetails);
  }
}
