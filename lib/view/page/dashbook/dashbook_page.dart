// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:dashbook/dashbook.dart';

// Project imports:
import 'package:katapultreader/domain/model/article_model.dart';
import 'package:katapultreader/view/page/articles/widgets/article_carousel_card.dart';
import 'package:katapultreader/view/page/articles/widgets/article_list_card.dart';

class DashbookPage extends StatefulWidget {
  @override
  _DashbookPageState createState() => _DashbookPageState();
}

class _DashbookPageState extends State<DashbookPage> {
  final _dashbook = Dashbook(
    usePreviewSafeArea: true,
  );

  final _articleModel = ArticleModel(
      title: 'Buchholzorgeln in Meck-Vorp',
      uri: Uri.parse(
          'https://katapult-mv.de/artikel/buchholzorgeln-in-meck-vorp'),
      dateline: 'Meck vs. Vorp',
      teaser:
          'Den originalen Buchholzer Orgelklang kann man fast nur in Vorp genießen. Das Ergebnis ist sehr eindeutig und beschert Vorp vorerst die Führung.',
      teaserImageUri: Uri.parse(
          'https://katapult-mv.de/media/pages/artikel/buchholzorgeln-in-meck-vorp/a22380881e-1630661626/mvv-buchholzorgeln-5zu6-360x.jpg'));

  @override
  Widget build(BuildContext context) {
    return _dashbook;
  }

  @override
  void initState() {
    super.initState();

    _dashbook
        .storiesOf('Articles')
        .decorator(CenterDecorator())
        .add('List card', (context) => ArticleListCard(_articleModel))
        .add('Carousel Card', (context) => ArticleCarouselCard(_articleModel));
  }
}
