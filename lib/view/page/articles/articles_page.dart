// Dart imports:

// Dart imports:

// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

// Project imports:
import 'package:katapultreader/view/page/articles/articles_controller.dart';
import 'package:katapultreader/view/page/articles/widgets/article_carousel_card.dart';
import 'package:katapultreader/view/page/articles/widgets/article_list_card.dart';

class ArticlesPage extends GetView<ArticlesController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      /*floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: controller.refreshArticles,
      ),*/
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              if (kDebugMode)
                IconButton(
                    onPressed: controller.onDashbookPressed,
                    icon: Icon(Icons.book_outlined)),
              Spacer(),
              Obx(() => ToggleButtons(
                      children: [
                        Icon(Icons.view_carousel_rounded),
                        Icon(Icons.grid_view_rounded),
                      ],
                      onPressed: controller.onViewLayoutButtonPressed,
                      isSelected:
                          controller.selectedViewLayoutMatrix.toList())),
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 48.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (kDebugMode)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: _HeaderRow(),
                ),
              if (kDebugMode) SizedBox(height: 16.0),
              Expanded(
                child: Obx(() {
                  switch (controller.viewLayout.value) {
                    case ViewLayout.carousel:
                      return _ArticlesCarousel();
                    case ViewLayout.list:
                      return _ArticlesList();
                  }
                }),
              ),
            ],
          ),
        ), //_ArticlesList(),
      ),
    );
  }
}

class _HeaderRow extends GetView<ArticlesController> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          "Katapult Reader",
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.start,
          style: TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}

class _ArticlesCarousel extends GetView<ArticlesController> {
  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => CarouselSlider.builder(
        options: CarouselOptions(
          enlargeCenterPage: true,
          enlargeStrategy: CenterPageEnlargeStrategy.scale,
          aspectRatio: 4 / 5,
        ),
        itemCount: state?.length,
        itemBuilder: (BuildContext context, int index, int pageViewIndex) {
          var articleModel = state![index];
          return ArticleCarouselCard(articleModel);
        }));
  }
}

class _ArticlesList extends GetView<ArticlesController> {
  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => StaggeredGridView.countBuilder(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        crossAxisCount: (Get.width / 250).ceil(),
        staggeredTileBuilder: (index) => StaggeredTile.fit(1),
        itemCount: state?.length,
        itemBuilder: (context, index) {
          var articleModel = state![index];
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
            child: GestureDetector(
              onLongPressStart: (_) =>
                  controller.onArticleLongPressed(articleModel),
              onTap: () => controller.onArticleTapped(articleModel),
              child: ArticleListCard(articleModel),
            ),
          );
        }));
  }
}
