// Package imports:
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

// Project imports:
import 'package:katapultreader/domain/model/article_model.dart';
import 'package:katapultreader/domain/repository/article_repository.dart';
import 'package:katapultreader/route/pages.dart';
import 'package:katapultreader/view/page/articles/widgets/article_hover_widget.dart';

enum ViewLayout { carousel, list }

class ArticlesController extends GetxController
    with StateMixin<List<ArticleModel>> {
  final ArticleRepository _articleRepository;

  ArticlesController(this._articleRepository);

  final viewLayout = Rx(ViewLayout.list);

  final selectedViewLayoutMatrix = [false, true].obs;

  @override
  void onInit() {
    super.onInit();

    append(() => _articleRepository.fetchArticles);
  }

  void onArticleLongPressed(ArticleModel article) {
    final hoverImageUrl = article.highResImageUri.toString();

    Get.dialog(ArticleHoverWidget(hoverImageUrl));
  }

  void onArticleTapped(ArticleModel article) {
    launch(article.uri.toString(), forceWebView: true);
  }

  void onViewLayoutButtonPressed(int index) {
    switch (index) {
      case 0:
        viewLayout.value = ViewLayout.carousel;
        selectedViewLayoutMatrix.value = [true, false];
        break;
      case 1:
        viewLayout.value = ViewLayout.list;
        selectedViewLayoutMatrix.value = [false, true];
        break;
    }
  }

  void onDashbookPressed() {
    Get.toNamed(Routes.DASHBOOK);
  }

  void refreshArticles() {
    append(() => _articleRepository.fetchArticles);
  }
}
