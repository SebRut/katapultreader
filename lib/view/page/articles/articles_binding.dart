// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:katapultreader/domain/repository/article_repository.dart';
import 'package:katapultreader/view/page/articles/articles_controller.dart';

class ArticlesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ArticleRepository());
    Get.lazyPut(() => ArticlesController(Get.find()));
  }
}
