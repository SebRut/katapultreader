// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:share_plus/share_plus.dart';

// Project imports:
import 'package:katapultreader/domain/model/article_model.dart';

class ArticleListCard extends StatelessWidget {
  final ArticleModel _articleModel;

  const ArticleListCard(this._articleModel);

  @override
  Widget build(BuildContext context) {
    return Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
        clipBehavior: Clip.antiAlias,
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          if (_articleModel.teaserImageUri != null)
            Image.network(
              _articleModel.teaserImageUri.toString(),
              fit: BoxFit.cover,
              width: double.maxFinite,
            ),
          Flexible(
            child: Padding(
                padding: EdgeInsets.fromLTRB(12.0, 16.0, 12.0, 0.0),
                child: Text(_articleModel.title,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.bold))),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                icon: Icon(Icons.share),
                onPressed: () => Share.share(
                    _articleModel.title + " — " + _articleModel.uri.toString()),
              )
            ],
          )
        ]));
  }
}
