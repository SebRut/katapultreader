// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

// Project imports:
import 'package:katapultreader/domain/model/article_model.dart';
import 'package:katapultreader/view/page/articles/articles_controller.dart';

class ArticleCarouselCard extends GetView<ArticlesController> {
  final ArticleModel _articleModel;

  ArticleCarouselCard(this._articleModel);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPressStart: (_) => controller.onArticleLongPressed(_articleModel),
      onTap: () => controller.onArticleTapped(_articleModel),
      child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
          clipBehavior: Clip.antiAlias,
          child: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
            if (_articleModel.teaserImageUri != null)
              Image.network(
                _articleModel.teaserImageUri.toString(),
                fit: BoxFit.cover,
                width: double.maxFinite,
              ),
            Expanded(
              child: Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 16.0, 20.0, 12.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(_articleModel.title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold)),
                      SizedBox(
                        height: 16.0,
                      ),
                      Expanded(
                        child: Text(_articleModel.teaser ?? "",
                            textAlign: TextAlign.start,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 8,
                            style: TextStyle(
                              fontSize: 16.0,
                            )),
                      ),
                    ],
                  )),
            ),
          ])),
    );
  }
}
