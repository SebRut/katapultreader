// Dart imports:
import 'dart:ui';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:get/get.dart';

class ArticleHoverWidget extends StatelessWidget {
  final String hoverImageUrl;

  ArticleHoverWidget(this.hoverImageUrl);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.back(),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 7.0),
        child: Center(
          child: InteractiveViewer(
            child: FractionallySizedBox(
              widthFactor: 0.8,
              child: Image.network(
                hoverImageUrl,
                fit: BoxFit.cover,
                loadingBuilder: (context, child, progress) {
                  if (progress == null) {
                    return child;
                  }

                  return FractionallySizedBox(
                    widthFactor: 0.5,
                    child: AspectRatio(
                        aspectRatio: 1.0,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircularProgressIndicator(),
                        )),
                  );
                },
                width: double.maxFinite,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
